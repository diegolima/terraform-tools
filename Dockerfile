FROM ubuntu:latest
LABEL maintainer Diego Lima <diego@diegolima.org>

ENV VERSION 0.12.12

RUN apt-get update \
	&& apt-get install -y \
		wget \
		unzip \
	&& rm -rf /var/lib/apt/lists/*
RUN wget https://releases.hashicorp.com/terraform/${VERSION}/terraform_${VERSION}_linux_amd64.zip -O /tmp/terraform.zip \
	&& unzip /tmp/terraform.zip -d /usr/local/bin \
	&& rm -f /tmp/terraform.zip

WORKDIR /opt
