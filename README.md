# Terraform Tools
### TLDR
A simple docker image for running Hashicorp's terraform client

## Basic Usage
From the directory containing the terraform templates that you want to work with:
```
docker run -ti -v $(pwd):/opt registry.gitlab.com/diegolima/terraform-tools terraform init
docker run -ti -v $(pwd):/opt registry.gitlab.com/diegolima/terraform-tools terraform apply
```


## Running multiple commands/getting a shell
```
$ docker run -ti -v $(pwd):/opt registry.gitlab.com/diegolima/terraform-tools bash
# terraform plan -out myplan
# terraform apply "myplan"
```

## Using the provided shellscript
A simple shellscript has been provided that should allow you to run the terraform command as
if locally installed. To use it follow these steps:
1. Copy the "terraform" script to your /usr/local/bin directory (you may need to do this using
sudo or as root):
```
# cp terraform /usr/local/bin/terraform
```
2. Make the script executable:
```
# chmod +x /usr/local/bin/terraform
```
3. Check that it works:
```
$ terraform version
```

### Shell
You can also run a shell using the provided script. This will automatically mount your current
directory into the working directory of the container:
```
$ terraform shell
```

### Limitations
- To run this as a non-root user you need to add that user to the docker group
- There are certain situations where terraform will fail, namely when a template includes files
that are not on a subdirectory of the current one. To apply such templates you may need to start
a shell inside the container from the top of the directory hierarchy.
